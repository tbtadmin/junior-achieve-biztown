<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Tampabay Times | Junior Achievement BizTown</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="bodyclasses">
            <div class="container">
                <?php include 'nav.php';?>
                <div class="row-fluid">
                    <div class="col-lg-4" id="left-sidebar-story-factory" class="" style="">
                        sidebar 1
                    </div><!-- /col 4 -->  
                    <div class="col-lg-8" id="right-sidebar-story-factory" class="" style="">
                        sidebar 2
                    </div><!-- /col 8 -->
                </div><!-- /row -->
                <div class="row-fluid">
                    <div class="col-lg-12" id="bottom-story-factory" class="" style="">&nbsp;</div>
                </div><!-- /row -->
            </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
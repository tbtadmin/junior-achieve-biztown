<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="/sites/all/themes/tbtseven/images/times_logo_horiz.png" alt="TampaBayTimes LOGO"/></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="image-uploader.php">Image Uploader</a></li>
            <li><a href="print-adcreator.php">Print Ad Creator</a></li>
            <li><a href="story-factory.php">Story Factory</a></li>
            <li><a href="file-manager.php">File Manager</a></li>
        </ul>
    </div><!--/.nav-collapse -->
</nav><!-- /nav -->

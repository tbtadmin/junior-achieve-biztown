<?php

// Allows deletion of any file within /my/files
// Usage:http://52.7.146.232/delete_file.php?file=

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);
//
$basedir = "/usr/share/nginx/html/sites/default/files/savedads";
$file_to_delete = $_REQUEST["file"];
$file_to_delete = explode('|', $file_to_delete);
$file = $file_to_delete[1];
$file = str_replace(' ', '_', $file);

$dir = $basedir . DIRECTORY_SEPARATOR . $file_to_delete[0] . DIRECTORY_SEPARATOR . '*.*';
$path = $basedir . DIRECTORY_SEPARATOR . $file_to_delete[0] . DIRECTORY_SEPARATOR . $file;

$files = glob($dir);
foreach ($files as $file) {
    if ($file != $ren = str_replace(" ", "_", $file)) {
        // there was a space
        rename($file, $ren);
        //echo "Renaming: [$file] to [$ren]<br />";
    }
}


if (file_exists($path)) {
    if (unlink($path)) {
        echo "success ";
        echo "<br />";
        header("Location: http://52.7.146.232/content/mediamanager?teacher=$file_to_delete[0]|ads");
    } else {
        echo "fail";
    }
} else {
    echo "file does not exist<br />";
    print $path;
}
?>

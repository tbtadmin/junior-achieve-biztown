<?php if ($logged_in): ?>
    <div id="bodyclasses">
            <div class="container" >
                <?php include 'nav.php';?>
                <div class="row-fluid row-eq-height">
                    <div class="col-lg-4" id="left-sidebar-image-uploader" class="" style="">
                        <img src="/sites/all/themes/tbtseven/images/hdr_imageUploader.png">
                        <div style="color: #fff; font-weight: bold; background-color: #204B5A; padding-left: 5px;">Step 1: Upload Image</div>
                            <p style="color: #fff; padding: 5px;">Choose image to upload</p>
                        <div style="color: #fff; font-weight: bold; background-color: #204B5A; padding-left: 5px;">Step 2: Add Credits</div>
                            <p style="color: #fff; padding: 5px;">Type in Photographer name and Caption for image.</p>
                        <div style="color: #fff; font-weight: bold; background-color: #204B5A; padding-left: 5px;">Step 3: Name and Save the Image</div>
                        <p style="color: #fff; padding: 5px;">Enter name and save the image.</p>  
                    </div><!-- /col 4 -->  
                    <div class="col-lg-8" id="right-sidebar-image-uploader" class="" style="">
                         <?php print render($page['content']); ?>  
                    </div><!-- /col 8 -->
                </div><!-- /row -->
                <div class="row-fluid">
                    <div class="col-lg-12" id="bottom-image-uploader" class="" style="">&nbsp;</div>
                </div><!-- /row -->
            </div>
    </div>
<?php else: ?>
        <?php drupal_goto('user/login'); ?>
<?php endif; ?>

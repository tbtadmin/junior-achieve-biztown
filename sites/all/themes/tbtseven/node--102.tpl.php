<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<?php if ($logged_in): ?>
<?php
$current_user = $user->uid;
$db = new PDO('mysql:host=localhost;dbname=jabiztown;charset=utf8', 'jabiztown', 'j4B1zt03n');

$e_image = "SELECT 
node.title,
file_managed.filename
FROM node 
JOIN field_revision_field_image ON node.nid = field_revision_field_image.entity_id
JOIN file_managed ON field_revision_field_image.field_image_fid = file_managed.fid
WHERE node.type = 'enterprise_image' and file_managed.uid = '1' ORDER BY node.title";
$image = $db->query($e_image);

$logo = "SELECT 
node.title,
file_managed.filename
FROM node 
JOIN field_revision_field_image ON node.nid = field_revision_field_image.entity_id
JOIN file_managed ON field_revision_field_image.field_image_fid = file_managed.fid
WHERE node.type = 'logo_uploader' ORDER BY node.title;";
$logo = $db->query($logo);

?>

    <div id="bodyclasses">
            <div class="container" >
                <?php include 'nav.php';?>
                <div class="row-fluid row-eq-height">
                    <div class="col-lg-4" id="left-sidebar-print-ad-creator" class="" style="">
                        <img src="/sites/all/themes/tbtseven/images/hdr_printAdCreator.png">
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 1: Choose a Photo</div>
                        <div style="padding: 5px;">
                            <?php
                                print "<select name='ad_image' id='ad_image'>";
                                    print "<option value=''><----- Select One -----></option>";
                                        while($row = $image->fetch()) {
                                            print "<option value='" . $row['filename'] . "'>" . ucfirst($row['title']) . "</option>";
                                        }
                                print "</select>";
                            ?>
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 2: Choose the Advertiser's Logo</div>
                        <div style="padding: 5px;">
                            <?php
                                print "<select name='logo_image' id='logo_image'>";
                                    print "<option value=''><----- Select One -----></option>";
                                        while($row = $logo->fetch()) {
                                            print "<option value='" . $row['filename'] . "'>" . $row['title'] . "</option>";
                                        }
                                print "</select>";
                            ?>    
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 3: Choose Layout</div>
                        <div style="padding: 5px;">
                            <select name="layout" id="layout">
                                <option value="wide">Wide Ad</option>
                                <option value="tall">Tall Ad</option>
                            </select>
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 4: Type Your Headline</div>
                        <div style="padding: 5px;">
                            <input type="text" name="headline" id="headline" required size="57" style="height:20px; border: 1px solid #000;">
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 5: Type Your Ad Copy</div>
                        <div style="padding: 5px;">
                            <textarea name="adcopy" id="adcopy" required rows="4" cols="45" style="border: 1px solid #000;"></textarea>
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 6: Choose Fonts and Sizes</div>
                        <div style="padding: 5px;">
                            <label>Headline 
                                <select name="headline_font" id="headlinefont">
                                    <option value="arialbd.ttf">Arial</option>
                                    <option value="timesbd.ttf">Times</option>
                                </select>
                                <select name="headline_size" id="headlinesize">
                                    <option value="smallest">Smallest</option>
                                    <option value="small">Small</option>
                                    <option value="normal">Normal</option>
                                    <option value="large">Large</option>
                                    <option value="largest">Largest</option>
                                </select>    
                            </label>
                            <label>Ad Copy 
                                <select name="adcopy_font" id="adcopyfont">
                                    <option value="arial.ttf">Arial</option>
                                    <option value="times.ttf">Times</option>
                                </select>
                                <select name="adcopy_size" id="adcopysize">
                                    <option value="smallest">Smallest</option>
                                    <option value="small">Small</option>
                                    <option value="normal">Normal</option>
                                    <option value="large">Large</option>
                                    <option value="largest">Largest</option>
                                </select>    
                            </label>
                        </div>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 7: Check Advertiser Contract </div>
                        <p style="color: #fff; padding: 5px;">Make sure you have a contract with this advertiser that says the advertiser wants to run this ad. Also, make sure the advertiser pays the bill!</p>
                        <div style="color: #fff; font-weight: bold; background-color: #2E441F; padding-left: 5px;">Step 8: Save Your Ad</div>
                        <div style="padding: 5px;">
                        <p style="color: #fff; padding: 5px;">Type in the name of your ad. Then click the save button.</p>
                        
                            <input type="text" name="saveAdname" id="saveAdname" style="border: 1px solid #000;">
                            <button onclick="mySavedAD()">Save Ad</button>
                        </div>
                    </div><!-- /col 4 -->  
                    <div class="col-lg-8" id="right-sidebar-print-ad-creator" class="" style="">
                    <img src="http://placehold.it/350x150" class="ad_placeholder" style="display: block; margin: 50px auto 50px auto; "/>
                    <script>
                        function displayVals() {
                            var adImage = $( "#ad_image" ).val();
                            var logoImage = $( "#logo_image" ).val();
                            var layout = $("#layout").val();
                            var headLine = $("#headline").val();
                            var adCopy = $("#adcopy").val();
                            var headlineFont = $("#headlinefont").val();
                            var headlineSize = $("#headlinesize").val();
                            var adcopyFont = $("#adcopyfont").val();
                            var adcopySize = $("#adcopysize").val();
                            var saveadName = $("#saveAdname").val();
               
                            $('img.ad_placeholder').attr('src',
                            'http://52.7.146.232/ads/create_ad.php?cboLayout='+layout+
                            '&cboHeadlineFontSize='+headlineSize+
                            '&cboCopyFontSize='+adcopySize+
                            '&cboSponsorImage='+logoImage+
                            '&cboAdImage='+adImage+
                            '&sharp=0&txtHeadline='+headLine+
                            '&txtWords='+adCopy+
                            '&cboHeadlineFont='+headlineFont+
                            '&cboCopyFont='+adcopyFont+
                            '&saveAdName='+saveadName+
                            '&currentUser='+<?=$current_user?>+
                            '');
        
                        }
                        $( "select" ).change( displayVals );
                        $("input").change( displayVals );
                        $("textarea").change( displayVals );
                        displayVals();
                        function mySavedAD() {
                            alert("Your ad is saved. please go to the file manager to see it.");
                        }
                    </script>      
                    </div><!-- /col 8 -->
                </div><!-- /row -->
                <div class="row-fluid">
                    <div class="col-lg-12" id="bottom-print-ad-creator" class="" style="">&nbsp;</div>
                </div><!-- /row -->
            </div>
    </div>
<?php else: ?>
        <?php drupal_goto('user/login'); ?>
<?php endif; ?>

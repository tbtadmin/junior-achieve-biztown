<?php if ($logged_in): ?>
    <?php
     error_reporting(0);
    $type = NULL;
    $teacher = NULL;
    $teacher = @$_REQUEST['teacher'];
    if ($teacher != NULL):
        $teacher_explode = explode('|', $teacher);
        $teacher = $teacher_explode[0];
        $type = $teacher_explode[1];
    else:
        $type = @$_REQUEST['type'];
    endif;
    $current_user = $user->uid;
    $db = new PDO('mysql:host=localhost;dbname=jabiztown;charset=utf8', 'jabiztown', 'j4B1zt03n');
    
    ?>
    <div id="bodyclasses">
        <div class="container " >
            <?php include 'nav.php'; ?>
            <div class="row-fluid row-eq-height">
                <div class="col-lg-4" id="left-sidebar-file-manager" class="" style="">
                    <img src="/sites/all/themes/tbtseven/images/hdr_fileManager.png">
                    <div style="color: #fff; font-weight: bold; background-color: #7F6900; padding-left: 5px;">Step 1: Choose Files to Show</div>
                    <div style="font-weight: bold; background-color: #7F6900; text-align: center; margin: 10px 55px 0px 55px;"><a href="?type=story" style="color: #fff;">Show Stories</a></div>
                    <div style="font-weight: bold; background-color: #7F6900; text-align: center; margin: 10px 55px 0px 55px;"><a href="?type=ads" style="color: #fff;">Show Ads</a></div>
                    <div style="font-weight: bold; background-color: #7F6900; text-align: center; margin: 10px 55px 0px 55px;"><a href="?type=enterprise_image" style="color: #fff;">Show Images</a></div> 
                </div><!-- /col 4 -->  
                <div class="col-lg-8" id="right-sidebar-file-manager" class="" style="">
                    <?php
                    if ($type == NULL) {
                        print '<h2> Choose a type to the left. </h2>';
                    } elseif ($type == 'story') {

                        $query = "SELECT 
                                node.title,
                                node.nid,
                                field_revision_field_author_name.field_author_name_value,
                                field_revision_body.body_value,
                                taxonomy_term_data.name,
                                node.uid
                              FROM node
                              JOIN field_revision_field_author_name ON node.nid = field_revision_field_author_name.entity_id
                              JOIN field_revision_body ON node.nid = field_revision_body.entity_id
                              JOIN field_revision_field_story_type ON node.nid = field_revision_field_story_type.entity_id
                              JOIN taxonomy_term_data ON taxonomy_term_data.tid = field_revision_field_story_type.field_story_type_tid  
                              WHERE node.type = 'story'";
                        $results = $db->query($query);
                        $rows = $results->fetchAll();
                        if ($current_user == '544') {
                            if ($teacher == NULL) {
                                $query = 'SELECT users.name, users.uid FROM users WHERE users.name LIKE "%bt%"';
                                $results = $db->query($query);
                                print '<h4>Teacher, please choose your schools login:</h4>';
                                print '<form action="/content/mediamanager?type=ads">';
                                print "<select name='teacher' id='teacher'>";
                                print "<option value=''><----- Select One -----></option>";
                                while ($row = $results->fetch()) {
                                    print "<option value='" . $row['uid'] . "|story'>" . $row['name'] . "</option>";
                                }
                                print "</select>";
                                print '<br />';
                                print '<input type="submit" value="Submit">';
                                print '</form>';
                            } else {
                                print '<h2> Student Stories</h2>';
                                print '<div class="panel-group" id="accordion">';
                                $studentcount = 0;
                                foreach ($rows as $row) {
                                    if ($row['uid'] == $teacher) {
                                        print '<div class="panel panel-default">';
                                        print '<div class="panel-heading">';
                                        print '<h4 class="panel-title">';
                                        print '<a data-toggle="collapse" data-parent="#accordion" href="#collapses' . $studentcount . '">' . $row['title'] . '</a>';
                                        print '</h4>';
                                        print '</div>';
                                        print '<div id="collapses' . $studentcount . '" class="panel-collapse collapse">';
                                        print '<div class="panel-body">';
                                        print '<p>' . $row['title'] . '</p>';
                                        print '<p>' . $row['field_author_name_value'] . '</p>';
                                        print '<p>' . $row['body_value'] . '</p>';
                                        print '<p> <a href="/node/' . $row['nid'] . '/edit?destination=content/mediamanager">edit</a>|<a href="/node/' . $row['nid'] . '/delete?destination=content/mediamanager">delete</a></p>';
                                        print '</div>';
                                        print '</div>';
                                        print '</div>';
                                        $studentcount++;
                                    }
                                }
                                print '</div>';
                            }
                        } else {
                            print '<h2> Student Stories</h2>';
                            print '<div class="panel-group" id="accordion">';
                            $studentcount = 0;
                            foreach ($rows as $row) {
                                if ($row['uid'] == $current_user) {
                                    print '<div class="panel panel-default">';
                                    print '<div class="panel-heading">';
                                    print '<h4 class="panel-title">';
                                    print '<a data-toggle="collapse" data-parent="#accordion" href="#collapses' . $studentcount . '">' . $row['title'] . '</a>';
                                    print '</h4>';
                                    print '</div>';
                                    print '<div id="collapses' . $studentcount . '" class="panel-collapse collapse">';
                                    print '<div class="panel-body">';
                                    print '<p>' . $row['title'] . '</p>';
                                    print '<p>' . $row['field_author_name_value'] . '</p>';
                                    print '<p>' . $row['body_value'] . '</p>';
                                    print '</div>';
                                    print '</div>';
                                    print '</div>';
                                    $studentcount++;
                                }
                            }
                            print '</div>';
                        }// end teacher
                        print '<h2> Other Available Stories </h2>';
                        print '<div class="panel-group" id="accordion">';
                        $teachercount = 0;
                        foreach ($rows as $row) {
                            if ($row['uid'] == '28') {
                                print '<div class="panel panel-default">';
                                print '<div class="panel-heading">';
                                print '<h4 class="panel-title">';
                                print '<a data-toggle="collapse" data-parent="#accordion" href="#collapset' . $teachercount . '">' . $row['title'] . '</a>';
                                print '</h4>';
                                print '</div>';
                                print '<div id="collapset' . $teachercount . '" class="panel-collapse collapse">';
                                print '<div class="panel-body">';
                                print '<p>' . $row['title'] . '</p>';
                                print '<p>' . $row['field_author_name_value'] . '</p>';
                                print '<p>' . $row['body_value'] . '</p>';
                                print '</div>';
                                print '</div>';
                                print '</div>';
                                $teachercount++;
                            }
                        }
                        print '</div>';
                    } elseif ($type == 'ads') {

                        if ($current_user == '544' || $current_user == '28') {
                            if ($teacher == NULL) {
                                $query = 'SELECT users.name, users.uid FROM users WHERE users.name LIKE "%bt%"';
                                $results = $db->query($query);
                                print '<h4>Teacher, please choose your schools login:</h4>';
                                print '<form action="/content/mediamanager?type=ads">';
                                print "<select name='teacher' id='teacher'>";
                                print "<option value=''><----- Select One -----></option>";
                                while ($row = $results->fetch()) {
                                    print "<option value='" . $row['uid'] . "|ads'>" . $row['name'] . "</option>";
                                }
                                print "</select>";
                                print '<br />';
                                print '<input type="submit" value="Submit">';
                                print '</form>';
                            } else {

                                print '<h2> Student Ads </h2>';
                                // open this directory 
                                $dir = "/usr/share/nginx/html/sites/default/files/savedads/$teacher/";

                                // Open a directory, and read its contents
                                if ($handle = opendir($dir)) {
                                    print '<ul class="rig columns-3" style="margin-right:20px;">';
                                    while (false !== ($entry = readdir($handle))) {
                                        if ($entry != "." && $entry != "..") {
                                            $path = "$teacher|$entry";
                                            print '<li>';
                                            print '<a class="thumb" href="/sites/default/files/savedads/' . $teacher . '/' . $entry . '" title="' . $entry . '" target="_new">';
                                            print '<img src="/sites/default/files/savedads/' . $teacher . '/' . $entry . '" />';
                                            print '<h3>' . $entry . '</h3>';
                                            print '</a>';
                                            print '<h3><a href="http://52.7.146.232/delete.php?file='.$path.'">delete</a></h3>';
                                            print '</li>';
                                        }
                                    }
                                    print '</ul>';
                                    closedir($handle);
                                }
                            }// student null    
                        } else {


                            print '<h2> Student Ads </h2>';
                            // open this directory 
                            $dir = "/usr/share/nginx/html/sites/default/files/savedads/$current_user/";

                            // Open a directory, and read its contents
                            if ($handle = opendir($dir)) {
                                print '<ul class="rig columns-3" style="margin-right:20px;">';
                                while (false !== ($entry = readdir($handle))) {
                                    if ($entry != "." && $entry != "..") {
                                        print '<li>';
                                        print '<a class="thumb" href="/sites/default/files/savedads/' . $current_user . '/' . $entry . '" title="' . $entry . '" target="_new">';
                                        print '<img src="/sites/default/files/savedads/' . $current_user . '/' . $entry . '" />';
                                        print '<h3>' . $entry . '</h3>';
                                        print '</a>';
                                        if($current_user == '28'):
                                        print '<h3><a href="http://52.7.146.232/delete.php?file=28|'.$entry.'">delete</a></h3>';    
                                        endif;
                                        print '</li>';
                                    }
                                }
                                print '</ul>';
                                closedir($handle);
                            }
                        }// teacher else   
                        print '<h2>Other Available Ads </h2>';
                        // open this directory 
                        $dir = "/usr/share/nginx/html/sites/default/files/savedads/28/";

                        // Open a directory, and read its contents
                        if ($handle = opendir($dir)) {
                            print '<ul class="rig columns-3" style="margin-right:20px;">';
                            while (false !== ($entry = readdir($handle))) {
                                if ($entry != "." && $entry != "..") {
                                    print '<li>';
                                    print '<a class="thumb" href="/sites/default/files/savedads/28/' . $entry . '" title="' . $entry . '" target="_new">';
                                    print '<img src="/sites/default/files/savedads/28/' . $entry . '" />';
                                    print '<h3>' . $entry . '</h3>';
                                    print '</a>';
                                    if($current_user == '28'):
                                        print '<h3><a href="http://52.7.146.232/delete.php?file=28|'.$entry.'">delete</a></h3>';    
                                    endif;
                                    print '</li>';
                                }
                            }
                            print '</ul>';
                            closedir($handle);
                        }
                    } elseif ($type == 'enterprise_image') {
                        $query = "SELECT 
                            node.title,
                            node.nid,
                            file_managed.filename,
                            node.uid
                          FROM node 
                          JOIN field_revision_field_image ON node.nid = field_revision_field_image.entity_id
                          JOIN file_managed ON field_revision_field_image.field_image_fid = file_managed.fid
                          WHERE node.type = 'enterprise_image'";
                        $results = $db->query($query);
                        $rows = $results->fetchAll();
                        if ($current_user == '544') {
                            if ($teacher == NULL) {
                                $query = 'SELECT users.name, users.uid FROM users WHERE users.name LIKE "%bt%"';
                                $results = $db->query($query);
                                print '<h4>Teacher, please choose your schools login:</h4>';
                                print '<form action="/content/mediamanager?type=ads">';
                                print "<select name='teacher' id='teacher'>";
                                print "<option value=''><----- Select One -----></option>";
                                while ($row = $results->fetch()) {
                                    print "<option value='" . $row['uid'] . "|enterprise_image'>" . $row['name'] . "</option>";
                                }
                                print "</select>";
                                print '<br />';
                                print '<input type="submit" value="Submit">';
                                print '</form>';
                            } else {
                                print '<h2>Student Images </h2>';
                                print '<ul class="rig columns-3" style="margin-right:20px;">';
                                foreach ($rows as $row) {
                                    if ($row['uid'] == $teacher) {
                                        print '<li>';
                                        print '<a class="thumb" href="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" target="_new">';
                                        print '<img src="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" />';
                                        print '</a>';
                                        print '<h3>' . $row['title'] . '</h3>';
                                        print '<h3 ><a href="/node/' . $row['nid'] . '/delete?destination=content/mediamanager">delete</a></h3>';
                                        print '</li>';
                                    }
                                } //end foreach
                                print '</ul>';
                            }
                        } else {

                            print '<h2>Student Images </h2>';
                            print '<ul class="rig columns-3" style="margin-right:20px;">';
                            foreach ($rows as $row) {
                                if ($row['uid'] == $current_user) {
                                    print '<li>';
                                    print '<a class="thumb" href="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" target="_new">';
                                    print '<img src="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" />';
                                    print '</a>';
                                    print '<h3>' . $row['title'] . '</h3>';

                                    print '</li>';
                                }
                            } //end foreach
                            print '</ul>';
                        }

                        print '<h2>Other Available Images</h2>';
                        print '<ul class="rig columns-3" style="margin-right:20px;">';
                        foreach ($rows as $row) {
                            if ($row['uid'] == '28') {
                                print '<li>';
                                print '<a class="thumb" href="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" target="_new">';
                                print '<img src="/sites/default/files/ad_story_images/' . $row['filename'] . '" title="' . $row['title'] . '" />';
                                print '<h3>' . $row['title'] . '</h3>';
                                print '</a>';
                                print '</li>';
                            }
                        } //end foreach
                        print '</ul>';
                    }// enterprises images
                    ?> 
                </div><!-- /col 8 -->
            </div><!-- /row -->
            <div class="row-fluid">
                <div class="col-lg-12" id="bottom-file-manager" class="" style="">&nbsp;</div>
            </div><!-- /row -->
        </div>
    </div>
<?php else: ?>
    <?php drupal_goto('user/login'); ?>
<?php endif; ?>

<?php if ($logged_in): ?>
    <div id="bodyclasses">
            <div class="container" >
                <?php include 'nav.php';?>
                <div class="row-fluid row-eq-height">
                    <div class="col-lg-4" id="left-sidebar-story-factory" class="" style="">
                        <img src="/sites/all/themes/tbtseven/images/hdr_storyFactory.png">
                        <div style="color: #fff; font-weight: bold; background-color: #6D3136; padding-left: 5px;">Before You Begin</div>
                            <p style="color: #fff; padding: 5px;">If you are going to cut-and-paste from a document, go open up that file so you will be ready to start.</p>
                        <div style="color: #fff; font-weight: bold; background-color: #6D3136; padding-left: 5px;">Step 1: Enter your Headline</div>
                            <p style="color: #fff; padding: 5px;">Type in your headline in the box on the right marked "Headline".</p>
                        <div style="color: #fff; font-weight: bold; background-color: #6D3136; padding-left: 5px;">Step 2: Enter the author's name</div>
                            <p style="color: #fff; padding: 5px;">Type in the name of the person who wrote the story in the box on the right marked "Author Name".</p>
                        <div style="color: #fff; font-weight: bold; background-color: #6D3136; padding-left: 5px;">Step 3: Enter the story copy</div>
                            <p style="color: #fff; padding: 5px;">If you are cutting and pasting from a document, paste the copied text into the box on the right marked "Story". <br />
        If you are not cutting and pasting, go ahead and type in the text of your story in the box on the right marked "Story".
                            </p>
                        <div style="color: #fff; font-weight: bold; background-color: #6D3136; padding-left: 5px;">Step 4: Save Your Story</div> 
                    </div><!-- /col 4 -->  
                    <div class="col-lg-8" id="right-sidebar-story-factory" class="" style="">
                         <?php print render($page['content']); ?>  
                    </div><!-- /col 8 -->
                </div><!-- /row -->
                <div class="row-fluid">
                    <div class="col-lg-12" id="bottom-story-factory" class="" style="">&nbsp;</div>
                </div><!-- /row -->
            </div>
    </div>
<?php else: ?>
        <?php drupal_goto('user/login'); ?>
<?php endif; ?>

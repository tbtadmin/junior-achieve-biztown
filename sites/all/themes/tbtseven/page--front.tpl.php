  <div id="branding" class="clearfix">
    <?php print $breadcrumb; ?>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="page-title"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php print render($primary_local_tasks); ?>
  </div>
  <br />
   <div id="jaBizFrontPage">
   <div class="jaBizMasthead">
   <a href="http://tampabay.com" target="_blank"><img style="vertical-align:middle; margin: 0px 0px 0px 20px" src="http://www.tampabay.com/logos/images/images01/times_logo_stacked_blk_url.gif" width="200px" alt="TampaBayTimes logo"/></a>
   <titles class="mastheadText">Media Manager</titles>
   <img style="vertical-align:middle; margin: 0px 20px 0px 0px" src="/sites/all/themes/tbtseven/images/tbt_logo.png" alt="tbt logo"/>
   </div>
   <div style="position:relative;" class="buttonBox">
    <a href="node/add/enterprise-image" target="_self" class="mmbutton imgUpload"><p>Image Uploader</p></a>
    <a href="content/print-ad-creator" target="_self" class="mmbutton printAdCreator"><p>Print Ad Creator</p></a>
    <a href="node/add/story" target="_self" class="mmbutton storyFactory"><p>Story Factory</p></a>
    <a href="content/mediamanager" target="_self" class="mmbutton fileManager"><p>File Manager</p></a>
   </div>
   <div style="position:relative;" class="buttonBox" id="jalogout">
    <?php
    if($logged_in):
            print '<a href="/user/logout" >Logout</a> ';
        else:
            print '<a href="/user/login" >Login</a> ';
    endif;
    
    ?>
   </div>    
   </div>
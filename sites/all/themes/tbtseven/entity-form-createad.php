<div class="<?php print $classes; ?> container"<?php print $attributes; ?>>
    <h1>Test</h1>
    <?php print render($title_prefix); ?>
  <?php print render($title_suffix); ?>

  <?php print render($form); ?>
</div>
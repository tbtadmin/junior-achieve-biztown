<?php

// Test GD image manipulation

$filename = $_REQUEST['file'];
$width = $_REQUEST['width'];
if( $width <  10)
	$width = 100;
$sharp = @$_REQUEST['sharp'];


$origWidth = $origHeight = 0;

$image = getImage( $filename);

$percent = $width / $origWidth;


header( 'Content-Type: image/png');

// Convert to grayscale

if( $image && imagefilter( $image, IMG_FILTER_GRAYSCALE))
{
//	$image2 = imagescale( $image, $percent * $origWidth, $percent * $origHeight, IMG_NEAREST_NEIGHBOR);

	// Scale image to fit the layout

	$image2 = imagecreatetruecolor( $percent * $origWidth, $percent * $origHeight);
	imagecopyresampled( $image2, $image, 0, 0, 0, 0, 
		$percent * $origWidth, $percent * $origHeight,
		$origWidth, $origHeight);

	// Sharpen it to offset the typical large reduction in size 

	if( $sharp) {
	    $sharpenMatrix = array 
            ( 
                array(-1.1, -1, -1.1), 
                array(-1, 20, -1), 
                array(-1.1, -1, -1.1) 
            ); 
            $divisor = array_sum(array_map('array_sum', $sharpenMatrix));            
            $offset = 0; 
            imageconvolution($image2, $sharpenMatrix, $divisor, $offset); 
	}

	imagepng( $image2);
}


function getImage( $filename) {
	global $origWidth, $origHeight;
	$image_info = getimagesize($filename);
	$origWidth = $image_info[0];
	$origHeight = $image_info[1];
	$image_type = $image_info[2];
	if( $image_type == IMAGETYPE_JPEG ) {
		return imagecreatefromjpeg($filename);
	} elseif( $this->image_type == IMAGETYPE_GIF ) {
		return imagecreatefromgif($filename);
	} elseif( $this->image_type == IMAGETYPE_PNG ) {
		return imagecreatefrompng($filename);
	} 
}
?>

<?php

	/*
	 * Serve up an image containing a rendered ad based on relatively limited 
	 * user-specified criteria.
	 *
	 * Parmaters:
	 *	cboSponsorImage (filename incl extension)
	 *	cboAdImage (tricky "choose_image.gif" default!)
	 *	cboLayout (wide/tall)
	 *	txtHeadline
	 *	txtWords
	 *	cboHeadlineFont (timesbd.ttf, )
	 *	cboHeadlineFontSize (big, etc)
	 *	cboCopyFont (arialbd.ttf, )
	 *	cboCopyFontSize (normal, )
	 * 
	 *      saveAdName (OPTIONAL, will save ad image to the file specified.  If file exists or
	 *		save error occurs, this will be displayed in the rendered image this 
	 *		service returns. 
	 */
        //current user
        $current_user = @$_REQUEST['currentUser'];
    
	// Configure image paths
	$logoDir = "../sites/default/files/logos/";
	$photoDir = "../sites/default/files/ad_story_images/";
        $saveDir = "../sites/default/files/savedads/$current_user/";
        if (!is_dir($saveDir)){
            mkdir($saveDir, 0777, true);
        }
              
	

	$logoFile = $logoDir . $_REQUEST['cboSponsorImage'];
	$pictFile = $photoDir . $_REQUEST['cboAdImage'];
	$layout = $_REQUEST['cboLayout'];
	$headline = @$_REQUEST['txtHeadline'];
	$text = @$_REQUEST['txtWords'];
        

	$headFont = 'fonts/' . $_REQUEST['cboHeadlineFont']; 
	$headSize = @$_REQUEST['cboHeadlineFontSize'];
	$textFont = 'fonts/' . $_REQUEST['cboCopyFont'];
	$textSize = @$_REQUEST['cboCopyFontSize']; 

	// Sanitize save file name 
	$saveAdName = @$_REQUEST['saveAdName']; 
	$saveAdName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $saveAdName);
	$saveAdName = preg_replace("([\.]{2,})", '', $saveAdName);

	header( 'Content-Type: image/jpg');
//	echo "<pre>";

	// Configure the renderer

	$sharp = true;

	$fontSizes = array (
		"head" => array (
			"smallest" => 12,
			"small" => 16,
			"normal" => 25,
			"large" => 30,
			"largest" => 35
		),
		"text" => array (
			"smallest" => 8,
			"small" => 10,
			"normal" => 14,
			"large" => 16,
			"largest" => 18
		)
	);

	$layouts = array (
		"wide" => array (
			"width" => 578,
			"height" => 268,
			"headGeometry" => array (
				"top" => 8,
				"left" => 20,
				"bottom" => 45,
				"right" => 540
			),
			"pictGeometry" => array (
				"top" => 60,
				"left" => 10,
				"bottom" => 255,
				"right" => 260
			),
			"textGeometry" => array (
				"top" => 60,
				"left" => 280,
				"bottom" => 190,
				"right" => 575,
			),
			"logoGeometry" => array (
				"top" => 205,
				"left" => 275,
				"bottom" => 255,
				"right" => 560
			)
		),
		"tall" => array (
			"width" => 268,
			"height" => 578,
			"headGeometry" => array (
				"top" => 2,
				"left" => 10,
				"bottom" => 45,
				"right" => 265,
			),
			"pictGeometry" => array (
				"top" => 120,
				"left" => 10,
				"bottom" => 300,
				"right" => 258
			),
			"textGeometry" => array (
				"top" => 300,
				"left" => 20,
				"bottom" => 515,
				"right" => 250,
			),
			"logoGeometry" => array (
				"top" => 515,
				"left" => 12,
				"bottom" => 565,
				"right" => 258
			)
		)
	);

	$adImage = imagecreatetruecolor( $layouts[$layout]['width'], $layouts[$layout]['height']);
	$black = imagecolorallocate( $adImage, 0, 0, 0);
	$white = imagecolorallocate( $adImage, 255, 255, 255);

	imagefill($adImage, 0, 0, $white);

	// Place logo and photo images then sharpen before adding text

	placeImageWithGeometry( $adImage, $logoFile, $layouts[$layout]['logoGeometry']);
	placeImageWithGeometry( $adImage, $pictFile, $layouts[$layout]['pictGeometry']);

	// Sharpen to offset the typical large reduction in image size 
	if( $sharp) {
		$sharpenMatrix = array ( 
				array(-1.0, -1, -1.0), 
				array(-1, 20, -1), 
				array(-1.0, -1, -1.0) 
			); 
		$divisor = array_sum(array_map('array_sum', $sharpenMatrix));            
		$offset = 0; 
		imageconvolution($adImage, $sharpenMatrix, $divisor, $offset); 
	}

	// Place headline and body text

	// last param means headline is always centered, both tall and wide layout
	renderTextWithGeometry( $adImage, $headline, 
		$headFont, $fontSizes['head'][$headSize], $black, $layouts[$layout]['headGeometry'], 8, true); 

	// last param below means center, done automatically on tall layout
	renderTextWithGeometry( $adImage, $text, 
		$textFont, $fontSizes['text'][$textSize], $black, $layouts[$layout]['textGeometry'], 3, $layout == 'tall'); 

	// Slap a box around it (use lines to avoid fill issues)

	$lineThickness = 2;
	$width = $layouts[$layout]['width'];
	$height = $layouts[$layout]['height'];
	imagesetthickness( $adImage, $lineThickness);
	imageline( $adImage, 0, 1, $width - $lineThickness, 1, $black);
	imageline( $adImage, 1, 1, 1, $height - $lineThickness, $black);
	imageline( $adImage, $width - $lineThickness+1, 0, $width - $lineThickness +1, $height - $lineThickness, $black);
	imageline( $adImage, 0, $height - $lineThickness + 1, $width - $lineThickness+1, $height - $lineThickness + 1, $black);

	
	// Optionally save to file 
	if( $saveAdName > '') {
                $saveAdName = $saveDir.$saveAdName;
		$saveAdName .= '.jpg';
		if( file_exists($saveAdName) || imagejpeg( $adImage, $saveAdName) == false)
		{
			// Replace our carefully-rendered image with an error message.  
	
			imagefilledrectangle( $adImage, 0, 0, $width, $height, $black);

			$text = "Error saving file.  Try using a different save ad name.";		
			renderTextWithGeometry( $adImage, $text, 
				$textFont, $fontSizes['head'][$textSize], $white, $layouts[$layout]['headGeometry'], 10, true); 
		}
	}

	// Emit the resulting image as a jpeg.  PNG would technically be better but their ancient 
	// Quark Express layout tool might not understand PNG so we stick with what they get today.

	imagejpeg( $adImage);

/****************************************************************/

function placeImageWithGeometry( $destImage, $filename, $geometry) {

	list( $image, $origWidth, $origHeight) = getImage( $filename);

	// Convert to grayscale

	if( $image && imagefilter( $image, IMG_FILTER_GRAYSCALE))
	{
		// Scale image to fit the layout, centering within available space

		$width = $geometry['right'] - $geometry['left'];
		$height = $geometry['bottom'] - $geometry['top'];
		$percent = min( $width / $origWidth, $height / $origHeight);
		$hOffset = ($width - $percent * $origWidth) / 2;
		$vOffset = ($height - $percent * $origHeight) / 2;

		imagecopyresampled( $destImage, $image, 
			$geometry['left'] + $hOffset, $geometry['top'] + $vOffset, 0, 0, 
			$percent * $origWidth, $percent * $origHeight,
			$origWidth, $origHeight);

		return true;
	}
	return false;
};


function getImage( $filename) {
	global $origWidth, $origHeight;
	$image_info = getimagesize($filename);
	$origWidth = $image_info[0];
	$origHeight = $image_info[1];
	$image_type = $image_info[2];
	if( $image_type == IMAGETYPE_JPEG ) {
		return array( imagecreatefromjpeg($filename), $origWidth, $origHeight) ;
	} elseif( $image_type == IMAGETYPE_GIF ) {
		return array( imagecreatefromgif($filename ), $origWidth, $origHeight);
	} elseif( $image_type == IMAGETYPE_PNG ) {
		return array( imagecreatefrompng($filename), $origWidth, $origHeight);
	} 
}

function renderTextWithGeometry( $image, $main_text, $font, $main_text_size, $main_text_color, $geometry,
	$line_pad, $center = FALSE) {

	// With word wrap, as found on SO.  Not beautiful but quite functional.

	$mx = $geometry['right'] - $geometry['left'];  // small safety margin on width
	$my = $geometry['bottom'] - $geometry['top'];

	$main_text_x = ($mx/2); 

	$words = explode(' ', $main_text); 
	$lines = array($words[0]); 
	$currentLine = 0; 

	// Add words to each line as space permits
	for($i = 1; $i < count($words); $i++) 
	{ 
		$lineSize = imagettfbbox($main_text_size, 0, $font, $lines[$currentLine] . ' ' . $words[$i]); 
		if($lineSize[2] - $lineSize[0] < $mx) 
		{ 
			$lines[$currentLine] .= ' ' . $words[$i]; 
		} 
		else 
		{ 
			$currentLine++; 
			$lines[$currentLine] = $words[$i]; 
		} 
	} 
	$line_count = 1; 
	$line_height = 0;

	// Draw the lines, centering if necessary
	foreach ($lines as $line) 
	{ 
		$line_box = imagettfbbox($main_text_size, 0, $font, "$line"); 
		$line_width = $line_box[0]+$line_box[2]; 
		if( $line_height < 1)
			$line_height = $line_box[1]-$line_box[7]; 
		$line_margin = ($mx-$line_width)/2; 
		$line_y = (($line_height + $line_pad) * $line_count); 

		if( $center)
			$left = $geometry['left'] + ($geometry['right'] - $geometry['left'] - $line_width) / 2;
		else
			$left = $geometry['left'];

		imagettftext( $image, $main_text_size, 0, $left, $geometry['top'] + $line_y, 
			$main_text_color, $font, $line); 

		$line_count ++; 
	} 
}
?>
